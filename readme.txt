=== Get Email ===
Contributors: Jeremy Hermelin
Donate link: http://www.nittswebinar.com
Tags: email, squeeze page, catcher, email catcher, lightbox, sg-autorepondeur, sgar, autorepondeur, 
Requires at least: 3.0
Tested up to: 4.3
Stable tag: 
License: GPLv2 or later

== Description ==

Ajouter un formulaire de capture email SG-autorepondeur à votre site WordPress n'a jamais été aussi simple. 
Munissez vous simplement de vos identifiant client, et identifiant de liste et collez-les dans les cases prévus dans les réglages. 

Adding SG-autorepondeur form code to your website has never been easier. 
Simply copy and paste your client id and the list id from your sg-autorepondeur account and that's it. 

== Installation ==

1. Uploadez le dossier 'get-email' dans le dossier '/wp-content/plugins/' de votre installation WordPress
2. Avctivez le plugin 'Get-email' dans le menu WordPress 'Plugins'

1. Upload `get-email` directory to the `/wp-content/plugins/` directory or use plugin search - Admin > Plugins > Add new > Search for 'Get Email'.
2. Activate the plugin through the 'Plugins' menu in WordPress

= How can I get support? =

Si vous avez le moindre soucis pour installer, utiliser, configurer ce plugin, je reste à votre disposition. 
Faites moi aussi part de vos avis, retours, suggestions, afin d'améliorer ce plugin. 
Vous pouvez me contacter en envoyant un email à support@nittswebinar.com, ou sur http://www.nittswebinar.com/contact/, en précisant 'Get-email' dans le sujet. 

In any case, problem, or issue using this plugin, please contact me. 
Please also let me know if you have any idea to make this plugin better. 
You can email me at support@nittswebinar.com, or http://www.nittswebinar.com/contact/, with 'Get-email' in the subject field. 

== Changelog ==

= 1.0 =
* Initial release.
