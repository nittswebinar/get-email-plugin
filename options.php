<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' ); ?>

<div class="wrap">
  <?php if( isset($_GET['settings-updated']) ) { ?>
    <div id="message" class="updated">
        <p><strong><?php _e('Réglages sauvegardés.', 'ge') ?></strong></p>
    </div>
  <?php } ?>
  
  <h2><?php echo __('Get Email', 'ge'); ?></h2>
  <div>
  	<p><?php echo __('Ce plugin vous est offert par ', 'ge'); ?> <a href="http://www.nittswebinar.com" title="Nittswebinar">NITTS Webinar</a> </p>
  	<p><?php echo __("Get Email vous permet d'intégrer <b>facilement</b> un formulaire de capture email lié à votre compte <b>SG-autorepondeur</b> sur votre blog WordPress. "); ?></p>
  	<br/>
  	<hr />
  </div>
  <div>
  	<!-- Display the form as it will be-->
  	<div style="float: right; ">
  		<p><b><?php echo __('Aperçu du formulaire : ', 'ge'); ?></b></p>
  	  	<?php echo get_email_boxed_form() ?>
    </div>
   	<form method="post" action="options.php" role="form" id="get-email-options">
    <?php settings_fields('get-email-settings'); ?>
    
    <!--Emplacement formulaire-->
    <div>
    	<br/>
      	<h3><?php echo __('Où voulez-vous faire apparaître le formulaire : ', 'ge'); ?></h3>
      	<div>
			<p><?php echo __('Pour afficher le formulaire dans la barre latérale, utilisez le Widget "Capture email" (dans Apparence >> Widgets). ', 'ge'); ?></p>
    	</div>
      
      	<div>
        	<input id="get_email_lightbox_switch" type="checkbox" name="get_email_lightbox_switch" <?php if(get_option('get_email_lightbox_switch')=='on'): ?> checked="checked" <?php endif; ?>>
        	<label for="get_email_lightbox_switch" class="col-sm-4 control-label"><?php echo __('dans une lightbox', 'ge'); ?></label>
      	</div>
      	<br/>
      	<div>
    		<input id="get_email_article_bottom_switch" type="checkbox" name="get_email_article_bottom_switch" <?php if(get_option('get_email_article_bottom_switch')=='on'): ?> checked="checked" <?php endif; ?>>
    		<label for="get_email_article_bottom_switch" class="col-sm-4 control-label"><?php echo __('à la fin de chaque article', 'ge'); ?></label>
        </div>
      	<br/>
      	<div>
        	<input id="get_email_article_top_switch" type="checkbox" name="get_email_article_top_switch" <?php if(get_option('get_email_article_top_switch')=='on'): ?> checked="checked" <?php endif; ?>>
        	<label for="get_email_article_top_switch" class="col-sm-4 control-label"><?php echo __('au début de chaque article', 'ge'); ?></label>
        </div>
      	<br/>
      	<br/>
      	<input type="submit" class="button button-primary" value="<?php _e('Sauvegarder', 'ge'); ?>"/>
      	<br/><br/><br/>
      	<hr/>
    </div>
    <!--Emplacement formulaire-->
    
    <!--Connexion à sg-autorepondeur-->
    <div>  
      <h3><?php echo __('Connexion à votre compte SG-Autorepondeur', 'ge'); ?></h3>
      
      <p><?php echo __("Munissez vous de votre <b>identifiant de compte</b> SG-autorepondeur, ainsi que de l'<b>identifiant de la liste</b> sur laquelle vous souhaitez capturer vos contacts comme ci-dessous : "); ?></p>
  	  <div>
	  <img alt="où récupérer vos identifiants clients sg-autorepondeur" src="/wp-content/plugins/get-email/images/tuto-sg.png"/>
	  </div>
	  <br/>
      
      <div>
        <label for="sgar_client_id" class="col-sm-4 control-label"><?php echo __('Numéro de compte', 'ge'); ?></label>
        <input type="text" class="form-control" name="sgar_client_id" id="sgar_client_id" placeholder="Exemple: 6923 " value="<?php echo get_option('sgar_client_id'); ?>">
      </div>
      <br/>
      
      <div>
        <label for="sgar_list_id" class="col-sm-4 control-label"><?php echo __('Numéro de liste', 'ge'); ?></label>
        <input type="text" class="form-control" name="sgar_list_id" id="sgar_list_id" placeholder="Exemple: 39568 " value="<?php echo get_option('sgar_list_id'); ?>">
      </div>
      <br/>
      <br/>
      <input type="submit" class="button button-primary" value="<?php _e('Sauvegarder', 'ge'); ?>"/>
      <br/><br/>
      <hr/>
    </div>
    <!--Connexion à sg-autorepondeur-->
    
    <!--Reglages du formulaire-->
    <div>    
      <h3><?php echo __('Réglages du formulaire', 'ge'); ?></h3>
      <div>
        <label for="get_email_form_title"><?php echo __("Phrase d'accroche du formulaire", 'ge'); ?></label>
        <div>
          <textarea rows="2" cols="50" name="get_email_form_title" id="get_email_form_title" placeholder="Ex: Recevez votre guide gratuit"><?php echo get_option('get_email_form_title'); ?></textarea>
        </div>
      </div>
      <br/>
      
      <div>
        <label for="get_email_form_submit_button" class="col-sm-4 control-label"><?php echo __("Texte du bouton de validation", 'ge'); ?></label>
        <div>
          <textarea rows="4" cols="50" name="get_email_form_submit_button" id="get_email_form_submit_button" placeholder="Ex: Télécharger"><?php echo get_option('get_email_form_submit_button'); ?></textarea>
        </div>
      </div>
      <br/>
      
      <div>
        <input id="get_email_form_image_switch" type="checkbox" name="get_email_form_image_switch" <?php if(get_option('get_email_form_image_switch')=='on'): ?> checked="checked" <?php endif; ?>>
        <label for="get_email_form_email_switch"><?php echo __('Inclure une image dans le formulaire', 'ge'); ?></label>
		<br/>
        <label for="get_email_form_image"><?php echo __("Url de l'image à inclure", 'ge'); ?></label>
        <div>
          <input type="url" size="50" name="get_email_form_image" id="get_email_form_image" placeholder="Ex: http://votresite.com/wp-content/uploads/image.png" value="<?php echo get_option('get_email_form_image'); ?>">
        </div>
      </div>
      <br/>
      
      <div>
        <label for="sgar_redirection"><?php echo __('Url de redirection après inscription (facultatif)', 'ge'); ?></label>
        <div>
          <input type="url" size="50" name="sgar_redirection" id="sgar_redirection" placeholder="Ex: http://www.votresite.com/merci" value="<?php echo get_option('sgar_redirection'); ?>">
		</div>
      </div>
      <br/>
      
      <div>
        <label for="sgar_onlist_redirection"><?php echo __('Url de redirection si déjà inscrit (facultatif)', 'ge'); ?></label>
        <div>
          <input type="url" size="50" name="sgar_onlist_redirection" id="sgar_onlist_redirection" placeholder="Ex: http://www.votresite.com/merci" value="<?php echo get_option('sgar_onlist_redirection'); ?>">
        </div>
      </div>
      <br/>
      <br/>
      <input type="submit" class="button button-primary" value="<?php _e('Sauvegarder', 'ge'); ?>"/>
      <br/>
    </div>	  
    <!--Reglages du formulaire-->
    
    
    </form>
  </div>  
  <br/>
  <br/>
  <hr/>
  <br/>
  <div><?php echo __('Vous utilisez les webinaires ? Testez gratuitement la plateforme de webinaire dédiée aux francophones ==> ', 'ge'); ?> <a href="http://www.nittswebinar.com" title="Nittswebinar">NITTS Webinar</a> </div>
  <br/><br/>
 
</div>
<br/>