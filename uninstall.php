<?php 

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// If uninstall is not called from WordPress, exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}
 
delete_option('sgar_client_id');
delete_option('sgar_list_id');
delete_option('sgar_onlist_redirection'); 
delete_option('sgar_redirection');

delete_option('get_email_sidebar_switch'); 
delete_option('get_email_lightbox_switch'); 
delete_option('get_email_articles_switch'); 
delete_option('get_email_header_switch'); 
delete_option('get_email_footer_switch'); 

delete_option('get_email_form_title'); 
delete_option('get_email_form_submit_button'); 
delete_option('get_email_form_image_switch'); 
delete_option('get_email_form_image');

?>