<?php
/*
Plugin Name: Get Email
Plugin URI: http://www.nittswebinar.com
Description: Vous permet d'intégrer facilement un formulaire de capture email SG-autorepondeur
Version: 1.0.0
Author: Jeremy Hermelin
Author URI: http://startupernow.com/
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: /languages

Get Email is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
Get Email is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with Get Email. If not, see https://www.gnu.org/licenses/gpl-2.0.html
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

register_deactivation_hook(__FILE__, 'deactive_get_email');
register_activation_hook( __FILE__, 'activate_get_email' );
add_action( 'admin_init', 'register_get_email_plugin_settings' );


function activate_get_email(){	
	
}
function deactive_get_email() {

}
function register_get_email_plugin_settings(){

	register_setting('get-email-settings','get_email_sidebar_switch'); 
	register_setting('get-email-settings','get_email_lightbox_switch'); 
	register_setting('get-email-settings','get_email_article_bottom_switch'); 
	register_setting('get-email-settings','get_email_article_top_switch'); 
	register_setting('get-email-settings','get_email_footer_switch'); 
	
	register_setting('get-email-settings','sgar_client_id');
	register_setting('get-email-settings','sgar_list_id');	
	register_setting('get-email-settings','sgar_onlist_redirection'); 
	register_setting('get-email-settings','sgar_redirection');
	
	register_setting('get-email-settings', 'get_email_form_title'); 
	register_setting('get-email-settings', 'get_email_form_submit_button'); 
	register_setting('get-email-settings', 'get_email_form_image_switch'); 
	register_setting('get-email-settings', 'get_email_form_image');
}


function admin_menu_get_email() {

  global  $settings_page; 

  $settings_page	=	add_menu_page( __('Get Email', 'ge'), __('Get Email', 'ge'), 'manage_options', 'get_email_settings', 'options_page_get_email' );

}

function options_page_get_email() {

  include(WP_PLUGIN_DIR.'/get-email/options.php');  

}

function get_email_boxed_form() {
 $boxed_form = sprintf('<div><div style="background-color: rgba(220, 220, 220, 0.3); padding: 20px; max-width: 400px; margin: auto">
 	%s
 	</div></div>', sgar_form());
 	
 	return $boxed_form;  
}

function get_email_raw_form() {
	return sgar_form(); 
}

function sgar_form() {

  $sgar_client_id	=	get_option('sgar_client_id');
  $sgar_list_id     =   get_option('sgar_list_id'); 
  
  $get_email_form_submit_button = get_option('get_email_form_submit_button'); 
  $get_email_form_title = get_option('get_email_form_title'); 
  
  $get_email_form_image = ''; 
  // if user want to add an image to the form
  if (get_option('get_email_form_image_switch')=='on') {
  	  $get_email_form_image = sprintf('<img alt="" height="130" width="130" src="%s" style="float: left; "/>', get_option('get_email_form_image')); 
  }
  
  $sgar_redirection =   '';  
  // if user specified a redirection url
  if (get_option('sgar_redirection') != '') {
	  $sgar_redirection = sprintf('<input name="redirect" type="hidden" value="%s" />', get_option('sgar_redirection')); 
  }
  
  $sgar_onlist_redirection = ''; 
  // if user specified an onlist redirection url
  if (get_option('sgar_onlist_redirection') != '') {
  	  $sgar_onlist_redirection = sprintf('<input name="redirect_onlist" type="hidden" value="%s" />', get_option('sgar_onlist_redirection')); 
  }

  $sgar_form_code	=	sprintf('<img alt="" height="1" src="http://sg-autorepondeur.com/tauxouverture/form.gif?membreid=%s&amp;listeid=%s" width="1" style="margin: 0; padding: 0; " />
	<div style="text-align: center; ">
		%s
		<p><b>%s</b></p>
	</div>
	<br/>
	<form accept-charset="UTF-8" action="http://sg-autorepondeur.com/inscriptionabonne.php" enctype="utf-8" method="post" target="_self" >
		<div>
			<div style="text-align: center; ">
				<input name="email" required="" type="text" value="" placeholder="Entrez votre email" />
			</div>
			<br/>
			<div style="text-align: center; ">
				<input name="valider" type="submit" value="%s" style="" />
			</div>
		</div>
		<input name="listeid" type="hidden" value="%s" /><input name="membreid" type="hidden" value="%s" />
		%s %s
	</form>', $sgar_list_id, $sgar_client_id, $get_email_form_image, $get_email_form_title, $get_email_form_submit_button, $sgar_list_id, $sgar_client_id, $sgar_redirection, $sgar_onlist_redirection);

	return $sgar_form_code;
}

function get_email_article_top_form( $content ) {
	if (is_single()) {
		$content = sprintf('%s <br/> %s', get_email_boxed_form(), $content); 
	}
	
	return $content; 		
}

function get_email_footer_form() {
		$catcher = sprintf('%s %s', get_email_raw_form(), '<br/><br/>'); 
		echo $catcher; 
}

function get_email_lightbox_form() {
	$lightbox = sprintf('<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript">
var delaiavantaffichage = 7000; /* en milisecondes, 7000 est notre recommandation */
var nomducookie = "sgar_%s"; 
var valeurducookie = "lightbox"; 
var dureeducookie = 1; 
var dureeducookieapresvalidation = 10*365; 
</script>
<script type="text/javascript" src="http://sg-autorepondeur.com/fonctions/sgar_popover/fancybox/jquery.cookie.js"></script>
<script type="text/javascript" src="http://sg-autorepondeur.com/fonctions/sgar_popover/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="http://sg-autorepondeur.com/fonctions/sgar_popover/fancybox/jquery.fancybox-1.3.4.js"></script>
<link rel="stylesheet" type="text/css" href="http://sg-autorepondeur.com/fonctions/sgar_popover/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<div style="display:none;z-index:10;">
	<a href="#formulaire-sg-autorepondeur" id="activationautomatique" style="display:none;">
	<div id="formulaire-sg-autorepondeur" style="overflow:auto;margin-top:-1px;">
	<!-- début du code html du formulaire -->
	<div style="background-color: white; ">%s</div>
	<!-- fin du code html du formulaire -->
	</div>
	</a>
	<script src="http://sg-autorepondeur.com/fonctions/sgar_popover/fancybox/popover.js"></script>
</div>', get_option('sgar_client_id'), get_email_boxed_form()); 
 
	echo $lightbox; 
}

function get_email_article_bottom_form( $content ) {
	if (is_single()) {
		$content = sprintf('%s %s', $content, get_email_boxed_form()); 
	}

	return $content; 	
}

function get_email_sidebar_form() {
	
}

if (is_admin()) {	

  add_action('admin_menu', 'admin_menu_get_email');

}



add_action('init', 'display_get_email_form_code');

function display_get_email_form_code(){
	
	add_action('widgets_init', function(){
		register_widget('get_email_widget'); 
	}); 
	
	if (get_option('get_email_lightbox_switch')=='on') {
	
		add_action('wp_footer', 'get_email_lightbox_form', 100); 
		
	}
	
	if (get_option('get_email_article_bottom_switch')=='on') {
	
		add_filter('the_content', 'get_email_article_bottom_form'); 
		
	}
	
	if (get_option('get_email_footer_switch')=='on') {

		add_action('wp_footer', 'get_email_footer_form', 3);	
		
	}
	
	if (get_option('get_email_article_top_switch')=='on') {
		
		add_filter('the_content', 'get_email_article_top_form'); 
		
	}

}

// define get_email_widget
class get_email_widget extends WP_Widget {
	function __construct() {
		parent::__construct(
			'get_email_widget', 
			__('Capture email', 'ge'), 
			array('description' => __('Intégrez vos formulaires sg-autorepondeur facilement', 'ge'), )
		); 
	}
	
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		echo get_email_raw_form();
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		?>
		<p>
		<?php _e( "Intégrez facilement un formulaire de capture d'emails sg-autorepondeur dans votre barre latérale." ); ?>
		</p>
		<p>
		<?php _e( "Glissez simplement le widget à l'emplacement souhaité pour votre formulaire" ); ?>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		return $instance;
	}

} // class get_email_widget


add_action('widgets_init', 
	create_function('', 'return register_widget("get_email_widget"); ')
); 